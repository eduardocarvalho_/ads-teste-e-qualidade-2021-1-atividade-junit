package br.ucsal.ads20211.testedequalidade.atividade02;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import br.ucsal.ads20211.testequalidade.atividade02.Questao01;

public class Questao01Test {

	private static final String LINE_SEPARTOR = System.lineSeparator();

	@ParameterizedTest
	@ValueSource(ints = { 1, 2, 3, 4, 5 })
	public void exeTest(int n) {
		assertTrue(Questao01.verificarPrimo(n));
	}

	@Test
	public void exeTest2() {
		ByteArrayInputStream inFake = new ByteArrayInputStream("12".getBytes());
		System.setIn(inFake);
		int resultadoReal = Questao01.obterNumeroFaixa();
		assertTrue(resultadoReal >= 1 && resultadoReal <= 1000);
	}

	@Test
	public void exeTest3() {

		ByteArrayOutputStream outFake = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outFake));
		Questao01.exibirPrimoNaoPrimo(3, true);
		String resultadoAtual = outFake.toString();
		String resultadoEsperado = "O n�mero 3 � primo" + LINE_SEPARTOR;
		Assertions.assertEquals(resultadoAtual, resultadoEsperado);

//	TODO
	}
}
